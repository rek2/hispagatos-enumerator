# Hispagatos
# Enumerator
# by the hispagatos anarcho hacker collective

Bash script that runs most of the external enumeration with some logic
25/08/2017 refactored all the scan script, in the next days the report.



Why?
I hate automatization of Pentests or any type of computer hacking with auto-tools expecially
if they are not free/open source FLOSS.
But sometimes for CTF/Certifications and such, were does not matter to make "noise" on the target.. 
I'm building step by step with the most commont enumeration steps I do personally.
example: 
CTF is ok to make noise, on a pentest is not..



![Alt text](/hispagatos-enumeration.png?raw=true "Henumeration screenshot")


TOOLS YOU NEED INSTALLED
I recomend Blackarch GNU/Linux

- system with sudo
- txt2tags - pacaur -S txt2tags
- Xalan-c - pacaur -S community/xalan-c 
- nmap
- nikto
- exploit-db - searchsploit(exploit-db)
  - pacaur -S exploit-db-git
  - then chmod 775 /usr/share/exploit-db/searchexploit
  - sudo ln -sf /usr/share/exploit-db/searchsploit /usr/local/bin/searchsploit
  - sudo searchsploit -u
- fimap
- Dirb
- egrep
- yasuo
- aha (from AUR pacaur -S aha-git)
- figlet
- GoBuster
- Spaghetti - pacaur -R blackarch/spaghetti
- SecList - dicctionaries - yaourt -S aur/seclists-git
- blackarch/ruby-unf
- Cherrytree - pacaur -S cherrytree - OPTIONAL I use this along with hispagatos-enumerator to create -NOTES file and other details 
- your brain - do not be a script kiddie and run this tool as a must, use it only for speed
not because you can't do this simple task by yourself.. 

to run, modify the config.sh

```
vim config.sh
```

Then to each remote host run the enumeration.sh script.. you can do this in screen for more than one host or diff terminals. so yes it can run in paralell.
```
./enumeration.sh xx.xx.xx.xx
```

on **each** created host folder like XX.XX.XX.XX are the files created, create one called STEPS
and add your custom **notes* of what you did and what you found on that hosts.

```
vim XX.XX.XX.XX/STEPS
```

After all remote hosts you need are scanned then run the documentation.sh script
```
./documentation.sh
```




Fernandez Chris AKA ReK2
https://keybase.io/cfernandez

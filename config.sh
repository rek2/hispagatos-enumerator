#!/usr/bin/env bash

set -eo pipefail
ulimit -n 4090

############## Variables #############
WORKINGDIR=~/oscp-labs
TARGET="$@"
NMAPP="/usr/bin/nmap"
ENUM4LINUX="/bin/enum4linux"
NIKTO="/usr/bin/nikto"
SMBMAP="/bin/smbmap"
DIRB="/bin/dirb"
YASUO="/bin/yasuo" #install ruby-nmap
AHA="/bin/aha"
GOBUSTER="/bin/gobuster"
GOBUSTEROPTS="-s 200,204,301,302,307,403,500 -e"
CUTYCAPT="/bin/CutyCapt"
SEARCHSPLOIT="/usr/share/exploit-db/searchsploit"
SPAGHETTI="/bin/spaghetti"
# this below is the default blackarch install but is always old
#SEARCHSPLOIT="/usr/share/davscan/searchsploit"
SEARCHSPLOITOPTS='-w --exclude="/dos/"'
AHAOPTS='--black --word-wrap'
TARGETDIR="${WORKINGDIR}/${TARGET}"
TARGETNOTES="${TARGETDIR}/${TARGET}-NOTES"
FILES="${WORKINGDIR}/${TARGET}/files"
SS="${WORKINGDIR}/${TARGET}/screenshots"
DOCS="${WORKINGDIR}/${TARGET}/docs"
FIMAP="/bin/fimap"
DIC_APACHE="/home/rek2/herramientas/diccionarios/SecLists/Discovery/Web_Content/apache.txt"
DIC_IIS="/home/rek2/herramientas/diccionarios/SecLists/Discovery/Web_Content/iis.txt"
DIC_COMMON="/home/rek2/herramientas/diccionarios/SecLists/Discovery/Web_Content/common.txt"
DIC_TOP1000="/home/rek2/herramientas/diccionarios/SecLists/Discovery/Web_Content/Top1000-RobotsDisallowed.txt"
DELETE="true"
DISABLE_DIRBUSTER="false"
WEBPORTS=()
#####################################

#!/bin/bash
# ReK2 Fernandez Chris
# hispagatos hacking collective
# https://hispagatos.org 
# https://keybase.io/cfernandez

set -eo pipefail

source config.sh

nmap_syn_stealth() {
  sudo $NMAPP -Pn -p- -vv -T4 ${TARGET} -oA ${TARGETDIR}/${TARGET}-BASIC-Pn-allports
  Xalan -a ${TARGETDIR}/${TARGET}-BASIC-Pn-allports.xml  > ${TARGETDIR}/${TARGET}-BASIC-Pn-allports.html
}


nmap_version_scan() {
  sudo $NMAPP -Pn -T4 -sV -p- ${TARGET} -oA ${TARGETDIR}/${TARGET}-Version-allports
  Xalan -a ${TARGETDIR}/${TARGET}-Version-allports.xml  > ${TARGETDIR}/${TARGET}-Version-allports.html
}

find_open_tcpports() {
  open_tcpports=$(cat ${TARGETDIR}/${TARGET}-BASIC-Pn-allports.nmap | sed '/open/!d' | cut -d "/" -f 1 | paste -d, -s)
  echo "Found TCP PORTS OPEN: ${open_tcpports}" >> ${TARGETDIR}/index.html

}

generic_vuln_scan() {
  sudo $NMAPP -Pn -sV -O -T4 -p T:${open_tcpports} --script="default,vuln and not brute" ${TARGET} -oA ${TARGETDIR}/${TARGET}-VULN
  Xalan -a ${TARGETDIR}/${TARGET}-VULN.xml > ${TARGETDIR}/${TARGET}-VULN.html
}


find_web() {
  IFS=',' read -r -a ports <<< "${open_tcpports}"
  for port in "${ports[@]}"; do
    echo "testing if ${port} host a webserver"
    if [[ port == "443" ]]; then
      echo "+ Port ${port} may be a TLS port can't test with out handshake"
      echo "+ assuming is a webserver... check manually"
      WEBPORTS+=(${port})
    else
      if [[ $(curl  -m 100 -sI ${TARGET}:${port}) == *HTTP* ]]; then
        echo "+ Port ${port} host a webserver"
        WEBPORTS+=(${port})
      else
        echo "+ Port ${port} does NOT host a webserver"
      fi
    fi
  done
  echo "<p>Found ${#WEBPORTS[@]} PORTS WITH A WEBSERVER: ${WEBPORTS[*]} </p>" >> ${TARGETDIR}/index.html
}


scan_samba() {
  echo "Starting enumeration of SMB enum4linux..."
  $ENUM4LINUX -a ${TARGET} >  ${TARGETDIR}/${TARGET}-ENUM4LINUX || true
  cat ${TARGETDIR}/${TARGET}-ENUM4LINUX  |  aha --black > ${TARGETDIR}/${TARGET}-ENUM4LINUX.html
  echo "Starting enumeration of SMB with smbmap..."
  $SMBMAP -R -H ${TARGET} > ${TARGETDIR}/${TARGET}-SMBMAP || true
  cat ${TARGETDIR}/${TARGET}-SMBMAP | aha --black  > ${TARGETDIR}/${TARGET}-SMBMAP.html
  echo "Starting nmap smb scripts scan..."
  $NMAPP -Pn -p445,135,139 --script="smb-enum-domains,
                                     smb-enum-groups,
                                     smb-enum-processes,
                                     smb-enum-sessions,
                                     smb-enum-shares,
                                     smb-enum-users,
                                     smb-ls,
                                     smb-os-discovery,
                                     smb-security-mode,
                                     smb-server-stats,
                                     smb-system-info,
                                     smb-vuln-conficker,
                                     smb-vuln-cve2009-3103,
                                     smb-vuln-ms06-025,
                                     smb-vuln-ms07-029,
                                     smb-vuln-ms08-067,
                                     smb-vuln-ms10-054,
                                     smb-vuln-ms10-061" \
                                     ${TARGET} -oA ${TARGETDIR}/${TARGET}-all-SMB
  Xalan -a ${TARGETDIR}/${TARGET}-all-SMB.xml  > ${TARGETDIR}/${TARGET}-all-SMB.html
}

scan_web() {
  webportslist=$( IFS=$','; echo "${WEBPORTS[*]}" )
  echo "+ Starting HTTP script vulns scan... to ports ${webportslist}"
  sudo $NMAPP -T4 -Pn -p${webportslist} --script="http-* and not brute" ${TARGET} -oA ${TARGETDIR}/${TARGET}-all-HTTP

  Xalan -a ${TARGETDIR}/${TARGET}-all-HTTP.xml > ${TARGETDIR}/${TARGET}-all-HTTP.html
  
  sudo $YASUO -t 100 -f ${TARGETDIR}/${TARGET}-Version-allports.xml  -p ${webportslist} -b all > ${TARGETDIR}/${TARGET}-YASUO
  cat ${TARGETDIR}/${TARGET}-YASUO | aha --black > ${TARGETDIR}/${TARGET}-YASUO.html

  for port in "${WEBPORTS[@]}"; do
    echo "+ About run run Spaghetti and Nikto on Host: ${TARGET} and port: ${port}"
    if [ port == "443" ]; then
      $SPAGHETTI -u https://${TARGET}:$port --scan 0 > ${TARGETDIR}/${TARGET}-${port}-SPAGHETTI
      $NIKTO -ssl -host https://${TARGET} -port $port -output ${TARGETDIR}/${TARGET}-${port}-NIKTO.txt
    else
      $SPAGHETTI -u http://${TARGET}:$port --scan 0 > ${TARGETDIR}/${TARGET}-${port}-SPAGHETTI
      $NIKTO -host http://${TARGET} -port $port -output ${TARGETDIR}/${TARGET}-${port}-NIKTO.txt
    fi
    cat ${TARGETDIR}/${TARGET}-${port}-SPAGHETTI | aha --black > ${TARGETDIR}/${TARGET}-${port}-SPAGHETTI.html
    cat ${TARGETDIR}/${TARGET}-${port}-NIKTO.txt | aha --black > ${TARGETDIR}/${TARGET}-${port}-NIKTO.html
    sed -i 's/style="color:black/style="color:white"/g' ${TARGETDIR}/${TARGET}-${port}-SPAGHETTI.html
  done  
}

do_bruteforce() {
  for port in "${WEBPORTS[@]}"; do
    if [ port == "443" ]; then
      $DIRB https://${TARGET}:$port /usr/share/dirb/wordlists/vulns/apache.txt,/usr/share/dirb/wordlists/common.txt,/usr/share/dirb/wordlists/indexes.txt -r -o ${TARGETDIR}/${TARGET}-Dirb-${port} || true
      $CUTYCAPT --smooth --plugins=on --url=https://${TARGET}:${port} --out=${SS}/${TARGET}:${port}-rootpage.png
      $GOBUSTER -k -u https://${TARGET}:$port -w ${DIC_COMMON} ${GOBUSTEROPTS} -o  ${TARGETDIR}/${TARGET}-GoBuster-DIC_COMMON-${port} || true
      $GOBUSTER -k -u https://${TARGET}:$port -w ${DIC_TOP1000} ${GOBUSTEROPTS} -o  ${TARGETDIR}/${TARGET}-GoBuster-DIC_TOP1000-${port} || true
    else
      $DIRB http://${TARGET}:$port /usr/share/dirb/wordlists/vulns/apache.txt,/usr/share/dirb/wordlists/common.txt,/usr/share/dirb/wordlists/indexes.txt -r -o ${TARGETDIR}/${TARGET}-Dirb-${port} || true
      $CUTYCAPT --smooth --plugins=on --url=http://${TARGET}:${port} --out=${SS}/${TARGET}:${port}-rootpage.png
      $GOBUSTER -k -u http://${TARGET}:$port -w ${DIC_COMMON} ${GOBUSTEROPTS} -o  ${TARGETDIR}/${TARGET}-GoBuster-DIC_COMMON-${port} || true
      $GOBUSTER -k -u http://${TARGET}:$port -w ${DIC_TOP1000} ${GOBUSTEROPTS} -o  ${TARGETDIR}/${TARGET}-GoBuster-DIC_TOP1000-${port} || true
    fi

    cat ${TARGETDIR}/${TARGET}-Dirb-${port} | aha --black  > ${TARGETDIR}/${TARGET}-Dirb-${port}.html || true
    cat ${TARGETDIR}/${TARGET}-GoBuster-DIC_COMMON-${port} | aha --black  > ${TARGETDIR}/${TARGET}-GoBuster-DIC_COMMON-${port}.html || true
    cat ${TARGETDIR}/${TARGET}-GoBuster-DIC_TOP1000-${port} | aha --black > ${TARGETDIR}/${TARGET}-GoBuster-DIC_TOP1000-${port}.html || true
  done
}


do_fimap() {
  for port in "${WEBPORTS[@]}"; do
    $FIMAP -H -u http://${TARGET}:${port} -d 3 -w ${TARGETDIR}/${TARGET}-fimap-${port}-urls
    $FIMAP -C -b -D  -m -l ${TARGETDIR}/${TARGET}-fimap-${port}-urls > ${TARGETDIR}/${TARGET}-fimap-${port}-results || true
    cat ${TARGETDIR}/${TARGET}-fimap-${port}-urls | aha --black  > ${TARGETDIR}/${TARGET}-fimap-${port}-urls.html
    cat ${TARGETDIR}/${TARGET}-fimap-${port}-results |  aha --black > ${TARGETDIR}/${TARGET}-fimap-${port}-results.html
  done
}


scan_snmp() {
  $NMAPP -Pn -p161,162 --script="snmp-*" ${TARGET} -oA ${TARGETDIR}/${TARGET}-all-SNMP
  Xalan -a ${TARGETDIR}/${TARGET}-all-SNMP.xml > ${TARGETDIR}/${TARGET}-all-SNMP.html
}


search_exploitdb() {
  $SEARCHSPLOIT $SEARCHSPLOITOPS --nmap ${TARGETDIR}/${TARGET}-Version-allports.xml > ${TARGETDIR}/${TARGET}-exploit-list
  cat ${TARGETDIR}/${TARGET}-exploit-list | aha $AHAOPTS --title "Exploit Results" > ${TARGETDIR}/${TARGET}-exploit-list.html
}


scan_mysql() {
  $NMAPP -Pn -p3306 --script="mysql-* and not brute" --script-args=unsafe=1 ${TARGET} -oA ${TARGETDIR}/${TARGET}-all-MYSQL
  Xalan -a ${TARGETDIR}/${TARGET}-all-MYSQL.xml > ${TARGETDIR}/${TARGET}-all-MYSQL.html
}


go_delete() {
  sudo rm ${TARGETDIR}/*.nmap ${TARGETDIR}/*.gnmap ${TARGETDIR}/*-NOTES ${TARGETDIR}/*.html || true
  sudo rm ${TARGETDIR}/*.xml ${TARGETDIR}/*-ENUM4LINUX ${TARGETDIR}/*-exploit-list || true
  sudo rm ${TARGETDIR}/*-Dirb ${TARGETDIR}/*-fimap ${TARGETDIR}/*-NIKTO || true
}

show_banner() {
  figlet "Hispagatos Enumerator" -c| aha --black     > ${TARGETDIR}/index.html
  figlet -f digital "By ReK2" -c | aha --no-header  >> ${TARGETDIR}/index.html
  date | figlet -f digital -c | aha --no-header     >> ${TARGETDIR}/index.html
  echo "<pre>"                                      >> ${TARGETDIR}/index.html
  echo "############# Starting ###############"     >> ${TARGETDIR}/index.html
  echo "working directory: ${WORKINGDIR}"           >> ${TARGETDIR}/index.html
  echo "Target: ${TARGET}"                          >> ${TARGETDIR}/index.html
  echo "Target directory: ${TARGETDIR}"             >> ${TARGETDIR}/index.html
  echo "Target Notes: ${TARGETNOTES}"               >> ${TARGETDIR}/index.html
  echo "######################################"     >> ${TARGETDIR}/index.html
  echo ""                                           >> ${TARGETDIR}/index.html
  echo ""                                           >> ${TARGETDIR}/index.html
  echo "</pre>"                                     >> ${TARGETDIR}/index.html

  echo "<h1><center> GENERIC NOTES </center></h1>" >> ${TARGETNOTES}

  figlet "Hispagatos Enumerator" -c
  figlet "By ReK2" -f digital -c 
  date | figlet -f digital -c
  echo "############# Starting ###############"
  echo "working directory: ${WORKINGDIR}"
  echo "Target: ${TARGET}"
  echo "Target directory: ${TARGETDIR}"
  echo "Target Notes: ${TARGETNOTES}"
  echo "######################################"
}


create_index() {
  echo "<table border="1" align="center">"       >> ${TARGETDIR}/index.html
  echo "<caption><em>RAW RESULTS FROM SCANS AND ENUMERATION</em></caption>" \
                                                 >> ${TARGETDIR}/index.html

  for create in $(ls ${TARGETDIR}/*.html | cut -d "/" -f 6); do
    if [ ! $create == "index.html" ]; then
      echo "<tr>"                                >> ${TARGETDIR}/index.html
      echo "<td>"                                >> ${TARGETDIR}/index.html
      echo "<a href=\"${create}\">${create}</a>" >> ${TARGETDIR}/index.html
      echo "</td>"                               >> ${TARGETDIR}/index.html
      echo "</tr>"                               >> ${TARGETDIR}/index.html
    fi
  done
  echo "</table>"                                 >> ${TARGETDIR}/index.html

  sed -i 's/telnet:/http:/g'  ${TARGETNOTES}
  sed -i 's/telnet:/http:/g'  ${TARGETDIR}/index.html
}


main() {

if [ $# -eq 0 ] ||  [ -z "$1" ]; then
  echo "No arguments supplied"
  exit 1
fi

if [ ! -d  ${WORKINGDIR} ]; then
  echo "+ creating directory ${WORKINGDIR}"
  mkdir ${WORKINGDIR} 
fi

if [ ! -d  ${TARGETDIR} ]; then
  echo "+ creating directory ${TARGETDIR}" 
  mkdir ${TARGETDIR}
fi

if [ ! -f ${TARGETNOTES} ]; then
  echo "+ creating file ${TARGETNOTES}"
  touch ${TARGETNOTES} 
fi

if [ ! -d ${FILES} ]; then                                                                                                                                  
  echo "+ creating directory ${FILES}"
  mkdir ${FILES}
fi

if [ ! -d ${DOCS} ]; then
  echo "+ creating directory ${DOCS}"
  mkdir ${DOCS}
fi

if [ ! -d ${SS} ]; then
  echo "+ creating directory ${SS}"
  mkdir ${SS}
fi

if [ $DELETE == "true" ]; then
   go_delete
fi

show_banner
nmap_syn_stealth
find_open_tcpports
nmap_version_scan
generic_vuln_scan
find_web

if [[ ${open_tcpports} == *"445"* ]] || [[ ${open_tcpports} == *"139"* ]]; then
  scan_samba
fi

if [[ ${open_tcpports} == *"3306"* ]]; then
  scan_mysql
fi



if [ ! ${#WEBPORTS[@]} -eq 0 ]; then 
  scan_web
  do_bruteforce
  do_fimap
fi

if [[ ${open_tcpports} == *"161"* ]] || [[ ${open_tcpports} == *"162"* ]]; then
  scan_snmp
fi

search_exploitdb
create_index

xdg-open ${TARGETDIR}/index.html 
}


main "$@"
